import React,{useState,useEffect} from 'react'
import { View, Text } from 'react-native'
import { GiftedChat,Bubble,InputToolbar} from 'react-native-gifted-chat'
import firebase from '../firebaseConfig'
export default function ChatGroup({user,route}) {
    const [messages, setMessages] = useState([]);
     const {email} = route.params;
     const getAllMessages = async ()=>{
        const docid  = email > user.email ? user.email+ "-" + email : email+"-"+user.email 
        const querySanp = await firebase.firestore().collection('user-group')
        .doc(docid)
        .collection('messages')
        .orderBy('createdAt',"desc")
        .get()
       const allmsg = querySanp.docs.map(docSanp=>{
            return {
                ...docSanp.data(),
                createdAt:docSanp.data().createdAt.toDate()
            }
        })
        setMessages(allmsg)
     }
     useEffect(() => {
        // getAllMessages()
  
        const docid  = email > user.email ? user.email+ "-" + email : email+"-"+user.email 
          const messageRef = firebase.firestore().collection('user-group')
          .doc(docid)
          .collection('messages')
          .orderBy('createdAt',"desc")
  
        const unSubscribe =  messageRef.onSnapshot((querySnap)=>{
              const allmsg =   querySnap.docs.map(docSanp=>{
               const data = docSanp.data()
               if(data.createdAt){
                   return {
                      ...docSanp.data(),
                      createdAt:docSanp.data().createdAt.toDate()
                  }
               }else {
                  return {
                      ...docSanp.data(),
                      createdAt:new Date()
                  }
               }
                  
              })
              setMessages(allmsg)
          })
  
          return ()=>{
            unSubscribe()
          }
  
          
        }, [])

      const onSend =(messageArray) => {
        const msg = messageArray[0]
        const mymsg = {
            ...msg,
            sentBy:user.email,
            sentTo:email,
            createdAt:new Date()
        }
       setMessages(previousMessages => GiftedChat.append(previousMessages,mymsg))
       const docid  = email > user.email ? user.email+ "-" + email : email+"-"+user.email 
 
       firebase.firestore().collection('user-group')
       .doc(docid)
       .collection('messages')
       .add({...mymsg,createdAt:firebase.firestore.FieldValue.serverTimestamp()})
      }
    return (
        <View style={{flex:1,backgroundColor:"#f5f5f5"}}>
           <GiftedChat
                messages={messages}
                onSend={text => onSend(text)}
                user={{
                    _id: user.email,
                }}
                renderBubble={(props)=>{
                    return <Bubble
                    {...props}
                    wrapperStyle={{
                      right: {
                        backgroundColor:"#082e7c",

                      },
                      left: {
                        backgroundColor:"#C0C0C0",
                      }
                      
                    }}
                  />
                }}
                renderInputToolbar={(props)=>{
                    return <InputToolbar {...props}
                     placeholder="Digite sua mensagem..."                     
                     containerStyle={{borderTopWidth: 1.5, borderTopColor: '#082e7c'}} 
                     textInputStyle={{ color: "black" }}
                     />
                }}
                
                />
        </View>
    )
}