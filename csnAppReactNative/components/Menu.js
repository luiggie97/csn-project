import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button,Image} from 'react-native';
import { Container } from 'react-bootstrap';
import firebase from '../firebaseConfig';
import { ActivityIndicator } from 'react-native-paper';

class Menu extends React.Component{
   
    render(){    
        return(
            <View style={styles.container}>    
                <Text style={styles.text}>Seja Bem Vindo!</Text>            
                <View style={styles.viewButton}>
                    <Button full title='Chats   ' color="#082e7c" texstyle={styles.button} onPress={()=>{this.props.navigation.navigate('HomeScreen')}}/>
                </View>
                <View style={styles.viewButton}>
                    <Button full title='Grupos ' color="#082e7c" texstyle={styles.button} onPress={()=>{this.props.navigation.navigate('Groups')}}/>
                </View>
            </View>
        );  
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    viewIMG: {
      paddingBottom:40
    },
    image: {
      //backgroundColor:'black',
      resizeMode:'stretch',
      height:90,      
      width:200
    },
    text:{
      color:'#082e7c',
      fontSize:15
    },
    input:{
      width: 250,
      height:60,
      borderWidth: 1,
      borderColor:'#082e7c',
      marginTop: 10,
      borderRadius:5,
      padding:10
    },
    button:{
      borderWidth: 5,
      color:'red',
      borderColor:'red'
    },
    viewButton:{
      justifyContent:'flex-end',
      alignItems:'flex-end', 
      paddingTop:10
    }
  });
export default Menu;