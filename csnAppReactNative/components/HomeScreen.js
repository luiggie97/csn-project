import React,{useState,useEffect} from 'react'
import { View, Text ,Image,FlatList,StyleSheet,TouchableOpacity} from 'react-native'
import firebase from '../firebaseConfig'
import { ActivityIndicator } from 'react-native-paper';

export default function HomeScreen({user,navigation}) {
    const [loading,setLoading] = useState(false)    
    const [users,setUsers] = useState(null)
    const getUsers =async ()=>{
             const querySanp = await firebase.firestore().collection('users').where('email','!=',user.email).get()
             const allusers = querySanp.docs.map(docSnap=>docSnap.data())
            console.log(allusers)
             setUsers(allusers)
             setLoading(false)
    }

    useEffect(()=>{
        setLoading(true)
        getUsers()
    },[])

    const RenderCard = ({item})=>{
        console.log(item)
          return (
            <TouchableOpacity onPress={()=>navigation.navigate('chat',{name:item.name,email:item.email,
                status :typeof(item.status) =="string"? item.status : item.status.toDate().toString()
            })}>
              <View style={styles.mycard}>
                  <View>
                      <Text style={styles.text}>
                          {item.name}
                      </Text>
                      <Text style={styles.text}>
                          {item.email}
                      </Text>
                  </View>
              </View>
              </TouchableOpacity>
          )
    }
    if(loading){
        return(
          <View>
            <ActivityIndicator size="large" color="#082e7c"/>
         </View>
        );
        
      }else{
        return (
        
            <View style={{flex:1}}>
                <FlatList 
                  data={users}
                  renderItem={({item})=> {return <RenderCard item={item} /> }}
                  keyExtractor={(item)=>item.email}
                />
            </View>
        )
      }
    
}


const styles = StyleSheet.create({
   img:{width:60,height:60,borderRadius:30,backgroundColor:"green"},
   text:{
       fontSize:18,
       marginLeft:15,
       color:'#082e7c',
   },
   mycard:{
       flexDirection:"row",      
       borderRadius: 20,     
       color: 'blue',
       textDecorationColor:'blue',      
       margin:3,
       padding:6,
       backgroundColor:"white",
       borderBottomWidth:1,
       borderBottomColor:'grey'
   },
 });