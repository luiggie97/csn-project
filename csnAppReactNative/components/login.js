import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button,Image} from 'react-native';
import { Container } from 'react-bootstrap';
import firebase from '../firebaseConfig';
import { ActivityIndicator } from 'react-native-paper';

class Login extends React.Component{
  state= {password:'',email:'',passCorrect:'white',heightPass:1, loading:false};

  
  submitLogin(){
  this.setState({loading:true});
 firebase
    .auth()
    .signInWithEmailAndPassword(this.state.email, this.state.password)
    .then(() => {
      console.log('Login efetuado com sucesso!');
      
      this.setState({loading:false});
    }, err => {     
      console.log('Erro ao efetuar log-in: ', err);
      this.setState({passCorrect: 'red', heightPass: 40});
      this.setState({loading:false});
    });
  }  
    render(){ 
      if(this.state.loading){
        return(
          <View>
            <ActivityIndicator size="large" color="#082e7c"/>
         </View>
        );
        
      } else{
        return(
              <View style={styles.container}>
                <View style={styles.viewIMG}>
                  <Image style={styles.image} source={require('../assets/logo_csn.png')} />
                </View>                 
                 <Text style={styles.text}>Log-In</Text>
                 <Text style={{color:this.state.passCorrect, height:this.state.heightPass}}>Usuário ou senha incorretos!</Text>
                <TextInput style={styles.input} placeholder ='E-mail' onChangeText={(e)=>this.setState({email:e})} value={this.state.email}/>
                <TextInput style={styles.input} secureTextEntry={true} placeholder ='Senha' onChangeText={(e) => this.setState({password: e})} value={this.state.password}/>
                
                <View style={styles.viewButton}>
                    <Button full title='Entrar' color="#082e7c" texstyle={styles.button} onPress={()=>{this.submitLogin()}}/>
                </View>
                <StatusBar style="auto" />
            </View>
        );
      } 
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    viewIMG: {
      paddingBottom:40
    },
    image: {
      //backgroundColor:'black',
      resizeMode:'stretch',
      height:90,      
      width:200
    },
    text:{
      color:'#082e7c',
      fontSize:15
    },
    input:{
      width: 250,
      height:60,
      borderWidth: 1,
      borderColor:'#082e7c',
      marginTop: 10,
      borderRadius:5,
      padding:10
    },
    button:{
      borderWidth: 5,
      color:'red',
      borderColor:'red'
    },
    viewButton:{
      justifyContent:'flex-end',
      alignItems:'flex-end', 
      paddingTop:10
    }
  });
export default Login;