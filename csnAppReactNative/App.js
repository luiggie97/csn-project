import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Button} from 'react-native';
//import firebase from './firebaseConfig';
import {createStackNavigator} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from './components/login';
import Chat from './components/chat';
import HomeScreen from './components/HomeScreen';
import Menu from './components/Menu';
import firebase from './firebaseConfig';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Stack = createStackNavigator();
const Navigation= ()=>{
  const [user,setuser] = useState('')
  useEffect(()=>{
   const unregister =  firebase.auth().onAuthStateChanged(userExist=>{
      if(userExist){
       
        firebase.firestore().collection('users')
        .doc(userExist.email)
        .update({
          status:"online"
        })
        setuser(userExist)
      } 
 
      else setuser("")
    })

    return ()=>{
      unregister()
    }

  },[])
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: (user?true:false) }}>
      {user?
        <>
         <Stack.Screen name="Menu" component={Menu} />   
         <Stack.Screen name="Grupos"  options={{
             headerRight:()=><MaterialIcons
             name="logout"
          size={34}
          color="blue"
          style={{marginRight:10}}
          onPress={()=>{
               firebase.firestore().collection('users')
               .doc(user.email)
               .update({
                 status:'offline'
               }).then(()=>{
                    firebase.auth().signOut()
               })
             }}
           />,
          title:"Chats"
        }}> 
         {props => <HomeScreen {...props}  user={user} />}
        </Stack.Screen>
        <Stack.Screen name="HomeScreen"  options={{
             headerRight:()=><MaterialIcons
             name="logout"
          size={34}
          color="blue"
          style={{marginRight:10}}
          onPress={()=>{
               firebase.firestore().collection('users')
               .doc(user.email)
               .update({
                 status:'offline'
               }).then(()=>{
                    firebase.auth().signOut()
               })
             }}
           />,
          title:"Chats"
        }}> 
         {props => <HomeScreen {...props}  user={user} />}
        </Stack.Screen>
        <Stack.Screen name="chat" options={({ route }) => ({ title:<View><Text style={{color:"#082e7c",fontWeight:"bold"}}>{route.params.name}</Text>
        <Text style={{color:(route.params.status=="online"?"green":"gray")}}>{route.params.status}</Text></View> })}>
          {props => <Chat {...props} user={user} /> }
        </Stack.Screen>
       
        </>
        : 
        <>
        <Stack.Screen name="Login" component={Login} />         
        </>
        }
        
        
      </Stack.Navigator>     
    </NavigationContainer>
  );
}

export default Navigation;
