import {BrowserRouter as Router, Switch} from 'react-router-dom';
import {Route} from 'react-router';
import React from 'react';
import LoginComponent from '../pages/login/login';
import DashboardComponent from '../pages/dashboard/dashboard';
import SignupComponent from '../pages/signup/signup';
import ListUserComponent from '../pages/users/list-user';
import DefaultMessagesRootComponent from '../pages/messages/default-messages-root';
import UserGroupRootComponent from '../pages/users/user-group-root';
import ChatComponent from '../pages/messages/chat';
import ChatGroupComponent from '../pages/messages/chat-group';
import axios from 'axios';
import Navbar from './navbar/navbar';
import firebase from 'firebase'
require('firebase/auth')
class RouterComponent extends React.Component{
    state = {
        permited: false,        
    }; 
   getUser = async (email)=>{
        try{
          return firebase
          .firestore()
          .collection('users')
          .where("email","==",email).get();
        }catch(e){
          console.log(e)
        }
    } 
    componentWillMount = async () => {      
        var email = ''
        await firebase.auth().onAuthStateChanged(async _usr => {            
            email = _usr ;
        });
        console.log(email)
        if (email != '' && email != undefined)
        {
            var userFireStore = await this.getUser(email.email)
            var role = userFireStore.docs.map(docSnap=>docSnap.data().role)
            console.log(role)
            if(role == 'master')
            {
                this.setState({
                    permited: true ,
                   
              });
            }
        }
      }
    
    
    render(){     
        return(  
             <Router>                 
                <Navbar />                
                <Switch>
                    <div id='routing-container'>     
                        <Route exact path='/' component={LoginComponent} />     
                        <Route path='/dashboard' component={DashboardComponent}></Route>  
                        <Route path='/cadastrar' component={SignupComponent}></Route>                  
                        <Route path='/lista-usuarios' component={ListUserComponent}></Route>                          
                        <Route path='/chat' component={ChatComponent}></Route>
                        <Route path='/chat-group' component={ChatGroupComponent}></Route>  
                        { this.state.permited &&
                           <Route path='/default-messages' component={DefaultMessagesRootComponent}></Route>                           
                        }
                        { this.state.permited &&
                           <Route path='/user-group' component={UserGroupRootComponent}></Route>    
                        }
                        { !this.state.permited &&
                           <Route path='/default-messages' component={LoginComponent}></Route>                           
                        }
                        { !this.state.permited &&
                           <Route path='/user-group'component={LoginComponent}></Route>    
                        }
                    </div>
                </Switch>
            </Router>
        );
    }
}

export default RouterComponent;