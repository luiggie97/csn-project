import React, { useState, useEffect } from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import { Link } from 'react-router-dom';
import { SidebarData } from './sidebardata';
import logo from '../../../src/logo_csn.png';
import './navbar.css';
import { IconContext } from 'react-icons';
import { useHistory } from 'react-router-dom'
import firebase from 'firebase'
require('firebase/auth')

function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const [icon, setIcon] = useState(false);

  const showSidebar = () => setSidebar(!sidebar);
  const history = useHistory() 
  
  
  useEffect(() => {
    return history.listen((location) => { 
      if(location.pathname == '' || location.pathname == '/cadastrar'){
        setIcon(false);
      }else{
        setIcon(true);
      }    
       
    })    
  },[history]) 
  const getUser = async (email)=>{
    try{
      return firebase
      .firestore()
      .collection('users')
      .where("email","==",email).get();
    }catch(e){
      console.log(e)
    }
  }
  useEffect(async () => {
  var email = ''
   await firebase.auth().onAuthStateChanged(async _usr => {            
      email = _usr ;
   });
   console.log(email)
  if (email != '' && email != undefined)
  {
    var userFireStore = await getUser(email.email)
    var role = userFireStore.docs.map(docSnap=>docSnap.data().role)
    console.log(role)
    if(role == 'master')
    {
      console.log('passou')
      var elements = [].slice.call(document.getElementsByClassName('d-none'));
      for(let i = 0; i < elements.length; i++ ) {
          elements[i].classList.remove('d-none');
      }     
    }
  }
   
  },) 

  const signOutUser = async () => {
    try {
        await firebase.auth().signOut();        
    } catch (e) {
        console.log(e);
    } 
  }
  const doNothing = async () => {
  
  }
  return (
    <>
      <IconContext.Provider value={{ color: '#082e7c' }}>
        <div className='row'>
        <div className='navbar col-lg-12'>
          <div className='col-lg-3'>
              <Link to='#' className={icon?'menu-bars':'menu-bars'}>
            <FaIcons.FaBars  onClick={showSidebar} />
          </Link>
          </div>
          <div className='col-lg-6'></div>
          <div className='col-lg-3 align-right'>
            <img className='logo' src={logo}></img>  
          </div>                  
        </div>
        </div>
        </IconContext.Provider>
        <IconContext.Provider value={{ color: '#fff' }}>
        <nav className={sidebar ? 'nav-menu active' : 'nav-menu' } id='navbar-csn'>
          <ul className='nav-menu-items' onClick={showSidebar}>
            <li className='navbar-toggle'>
              <Link to='#' className='menu-bars'>
                <AiIcons.AiOutlineClose />
              </Link>
            </li>
            {SidebarData.map((item, index) => {
              return (
                <div>                 
                  <li key={item.title} className={item.cName} onClick={item.title =='Log-Out'?signOutUser:doNothing}>
                    <Link to={item.path}>
                      {item.icon}
                      <span>{item.title}</span>
                    </Link>
                  </li>
                </div>
              );
            })}           
          </ul>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;
