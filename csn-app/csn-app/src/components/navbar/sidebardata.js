import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as BiIcons from 'react-icons/bi';
export const SidebarData = [
  {
    title: 'Dashboard',
    path: '/Dashboard',
    icon: <AiIcons.AiFillHome />,
    cName: 'nav-text'
  },  
  /*{
    title: 'Lista de Usuários',
    path: '',
    icon: <IoIcons.IoMdPeople />,
    cName: 'nav-text'
  },*/
  {
    title: 'Histórico de Mensagens',
    path: '/chat',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Histórico de grupo de Mensagens',
    path: '/chat-group',
    icon: <IoIcons.IoIosPaper />,
    cName: 'nav-text'
  },
  {
    title: 'Log-Out',
    path: '',
    icon: <BiIcons.BiLogInCircle/>,
    cName: 'nav-text',
    onClick: 'signOutUser'
  },
  {
    title: 'Grupo de usuários',
    path: '/user-group',
    icon: <AiIcons.AiOutlineUsergroupAdd />,
    cName: 'nav-text d-none'
  },
  {
    title: 'Mensagens Padrões',
    path: '/default-messages',
    icon: <BiIcons.BiMessageSquareDetail/>,
    cName: 'nav-text d-none'
  }
];
