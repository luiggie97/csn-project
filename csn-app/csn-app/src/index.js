import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import RouterComponent from './components/routes';

const firebase = require("firebase");
require("firebase/firestore");
var config={
    apiKey: "AIzaSyDDvljudL8Hm4qYKBz1ee_1axkzmUklUpA",
    authDomain: "csn-project-91e05.firebaseapp.com",
    projectId: "csn-project-91e05",
    storageBucket: "csn-project-91e05.appspot.com",
    messagingSenderId: "708638836397",
    appId: "1:708638836397:web:e649785649d5ddc0266824",
    measurementId: "G-6828RS4WG8"
};
firebase.default.initializeApp(config);


ReactDOM.render(<RouterComponent></RouterComponent>, document.getElementById('root'));

serviceWorker.unregister();