import { Link } from 'react-router-dom';
import React from 'react';
import styles from './styles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import logo from '../../../src/logo_csn.png';

import firebase from 'firebase'
require('firebase/auth')

class LoginComponent extends React.Component{
    constructor() {
        super();
        this.state = {
          email: null,
          password: null,
          serverError: false
        };
      }
    
    render() {

        const { classes } = this.props;
    
        return (
          <main className={classes.main}  style={{ 
            backgroundImage:{logo} 
          }}>
            <CssBaseline/>
            <Paper className={classes.paper}>
              <Typography component="h1" variant="h5">
                Log-In
              </Typography>
              <form onSubmit={(e) => this.submitLogin(e)} className={classes.form}>
                <FormControl required fullWidth margin='normal'>
                  <InputLabel htmlFor='login-email-input'>E-mail</InputLabel>
                  <Input autoComplete='email' autoFocus onChange={(e) => this.userTyping('email', e)} id='login-email-input'></Input>
                </FormControl>
                <FormControl required fullWidth margin='normal'>
                  <InputLabel htmlFor='login-password-input'>Senha</InputLabel>
                  <Input autoComplete="current-password" type="password" onChange={(e) => this.userTyping('password', e)} id='login-password-input'></Input>
                </FormControl>
                <Button type='submit' fullWidth variant='contained' color='primary' className={classes.submit}>Entrar</Button>
              </form>
              { this.state.serverError ? 
                <Typography className={classes.errorText} component='h5' variant='h6'>
                  Usuário ou senha não conferem
                </Typography> :
                null
              }              
              <Link className={classes.signUpLink} to='/cadastrar'>Não possui conta?Registre-se!</Link>
            </Paper>
          </main>
        );
      }
      userTyping = (whichInput, event) => {
        switch (whichInput) {
          case 'email':
            this.setState({ email: event.target.value });
            break;
    
          case 'password':
            this.setState({ password: event.target.value });
            break;
    
          default:
            break;
        }
      }
    
      submitLogin = async (e) => {
        e.preventDefault(); 
    
        await firebase
          .auth()
          .signInWithEmailAndPassword(this.state.email, this.state.password)
          .then(() => {
            this.props.history.push({
              pathname: '/dashboard',                            
            });
          }, err => {
            this.setState({ serverError: true });
            console.log('Erro ao efetuar log-in: ', err);
          });
      };
    
    
}

export default withStyles(styles)(LoginComponent);