import * as React from 'react';
import {Container,Jumbotron} from 'react-bootstrap';
import firebase from 'firebase'
require('firebase/auth')

class DashboardComponent extends React.Component {  
  state={user:null}
  render(){  
  return (
    <Container >      
      <div style={{ height: 400, width: '100%', paddingLeft:120 }} className="c">
       <Jumbotron>
          <h1>Seja Bem Vindo {this.state.user}!</h1>
          <p>
            Sistema de Troca de mensagens CSN Mineração
          </p>
       </Jumbotron>
      </div>
    </Container>    
  );
  }

  componentWillMount = () => {
    firebase.auth().onAuthStateChanged(async _usr => {            
      let AuxUser = _usr.email.substring(0,_usr.email.indexOf("@"));            
        this.setState({user:AuxUser});
      if(!_usr)
        this.props.history.push('/');
  });
}
}
export default (DashboardComponent);