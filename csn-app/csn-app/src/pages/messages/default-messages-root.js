import React from "react";
import MessagesDefault from "./default-messages";

function DefaultMessagesRoot(){
    return (
        <div className="row">
            <div className="col-md-8 offset-md-2">
                <MessagesDefault></MessagesDefault>
            </div>
        </div>
    );
}

export default DefaultMessagesRoot;