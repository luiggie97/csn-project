import React,{useState,useEffect} from 'react'
import FlatList from 'flatlist-react';
import { Grid } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';
import { Dots } from "react-activity";
import "react-activity/dist/Dots.css";
import useStyles from './chatStyles';
import {Container} from 'react-bootstrap';
import { Button} from '@material-ui/core';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ChatScreen from './chatScreen';
import Avatar from '@material-ui/core/Avatar';
import firebase from 'firebase'
require('firebase/auth');


export default function ChatComponent() {
    const [loading,setLoading] = useState(false)    
    const [users,setUsers] = useState(null)
    const [user,setUser] = useState(null)
    const [userTo,setUserTo] = useState(null)
    const classes = useStyles();

    async function getUsers(){            
        const querySanp = await firebase.firestore().collection('users').where('email','!=',user.email).get()
        const allusers = querySanp.docs.map(docSnap=>docSnap.data())
        console.log(allusers) 
        setUsers(allusers)
        setLoading(false)                
    }

    async function verifyUser() {
        let Auxuser = await firebase.auth().currentUser

        if(Auxuser == null){
            firebase.auth().onAuthStateChanged(async _usr => {            
                let AuxUseraux = _usr;
                setUser(AuxUseraux);                
                if(!_usr)
                  this.props.history.push('/');
            });
        }
        else{
            setUser(Auxuser);  
        }
        console.log(Auxuser)                           
    }  
    useEffect(()=>{
        setLoading(true)
        verifyUser()
    },[])

    useEffect(()=>{         
        if(user !==null)
            getUsers()
    },[user])
   
    useEffect(()=>{
        let navbar = document.querySelector('#navbar-csn');
        let list = document.querySelector('#dashboard-container');
        if(list != null){
            if(navbar.classList.contains('active'))
                list.classList.add('d-none')
            else
                list.classList.remove('d-none')
        }
        
    })
    
    const RenderCard = ({item})=>{       
          return (
            <ListItem onClick={() =>setUserTo(item.email)}className={classes.listItem} >
              <ListItemAvatar>
                   <Avatar alt="Remy Sharp">{item.name[0]}</Avatar>
              </ListItemAvatar>
              <Grid>
                  <Grid>
                      <Typography className={classes.text}>
                        {item.name}
                      </Typography>
                      <Typography className={classes.text}>
                      {item.email}
                      </Typography>
                  </Grid>
              </Grid>              
              </ListItem>
          )
    }
    if(loading){
        return(
          <Grid>
            <Dots size="large" color="#082e7c"/>
         </Grid>
        );
        
      }else{
        return (

            <Container>      
                <div>            
                    <div className='row' >
                        <div className={classes.root} id='dashboard-container'>                          
                                    <List>
                                        {
                                        users == null?
                                        <ListItem>Nada</ListItem>
                                        :
                                            users.map((user,_index)=>{                            
                                                return(
                                                    <div key={_index}>
                                                        <RenderCard item={user} />
                                                        <hr/>
                                                    </div>                                
                                                )                           
                                            })
                                        
                                        }
                                    </List>
                            <Button className={classes.signOutBtn}>Deslogar</Button>
                        </div>                        
                        <div className={classes.mensagem}>
                        {
                            userTo == null
                            ?<div></div>
                            :
                            <ChatScreen
                                userBy={user} 
                                userTo={userTo}>
                            </ChatScreen>        
                        }        
                        </div>    
                     
                            
                    </div>        
                </div>
            </Container>   
            
        )
      }
    
}


