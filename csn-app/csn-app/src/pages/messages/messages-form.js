import React,{useState, useEffect} from "react";

const MessagesDefaultForm = (props) => {
const initialFieldValues = {
    name:'',
    message:''    
}

var [values, setValues] = useState(initialFieldValues)

useEffect( ()=>{    
    if(props.currentId=="" )
        setValues({
            ...initialFieldValues
        })
    else
        setValues({
            ...props.messagesObject[props.currentId]
        })
},[props.currentId, props.currentIdSearch,props.messagesObject])

const handleInputChange = e =>{
    var {name, value } = e.target
    setValues({
        ...values,
        [name]: value
    })
}
const handleFormSubmit = e =>{
    e.preventDefault();
    props.addOrEdit(values);   
}
    return (
      <form autoComplete="off" onSubmit={handleFormSubmit}>                
          <div className="form-row">
            <div className="form-group input-group col-md-12">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-file-signature"></i>
                    </div>
                </div>
                <input className="form-control" placeholder="Nome" name="name"
                    value={values.name}
                    onChange={handleInputChange}
                />
            </div>    
          </div>
          <div className="form-row">
            <div className="form-group input-group col-md-12">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-comment-alt"></i>
                    </div>
                </div>
                <textarea className="form-control" placeholder="Mensagem" name="message"
                    value={values.message}
                    onChange={handleInputChange}
                />
            </div>    
          </div>
          <div className="form-row">
            <div className="form-group col-md-3">
                <input type="submit" value={props.currentId==''?"Salvar":"Atualizar"} className="btn btn-primary btn-block"/>
            </div>    
          </div>
          
      </form>
    );
 }
 export default MessagesDefaultForm