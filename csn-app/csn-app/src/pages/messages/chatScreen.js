import React,{useState,useEffect} from 'react'
import Select from 'react-select'
import { Grid } from '@material-ui/core';
import useStyles from './chatStyles';
import { GiftedChat,Bubble,InputToolbar} from 'react-web-gifted-chat'
import TextField from '@material-ui/core/TextField';
import Send from '@material-ui/icons/Send';
import firebase from 'firebase'
require('firebase/auth');

export default function ChatScreen({userBy,userTo}) {
    const [messages, setMessages] = useState([]);
    const [user, setUser] = useState(userBy);
    const [email, setEmail] = useState(userTo)
    const [chatText, setChatText] = useState("")
    const [options, setOptions] = useState("")
    const [initialValue, setInitialValue] = useState('')
    const classes = useStyles();
     
     const getAllMessages = async ()=>{
        const docid  = email > user.email ? user.email+ "-" + email : email+"-"+user.email 
        const querySanp = await firebase.firestore().collection('chatrooms')
        .doc(docid)
        .collection('messages')
        .orderBy('createdAt',"desc")
        .get()
       const allmsg = querySanp.docs.map(docSanp=>{
            return {
                ...docSanp.data(),
                createdAt:docSanp.data().createdAt.toDate()
            }
        })
        setMessages(allmsg.reverse)
     }

     useEffect(()=>{
        setEmail(userTo)
     },[userTo])
     useEffect(async () => {

      /*const options = firebase.firestore().collection('chatrooms')
      
      .collection('messages')
      .orderBy('createdAt',"desc")*/
      //setOptions(options)
        // getAllMessages()

        const optionsAux = await firebase.firestore().collection('default-message').get()
       
            const options = optionsAux.docs.map(docSnap=>{
              const data = { 
                value: docSnap.id,
                label: docSnap.data().name,
                message:docSnap.data().message
              }          
              return data;
            })  
         
        setOptions(options)   
        const docid  = email > user.email ? user.email+ "-" + email : email+"-"+user.email 
          const messageRef = firebase.firestore().collection('chatrooms')
          .doc(docid)
          .collection('messages')
          .orderBy('createdAt',"desc")
  
        const unSubscribe =  messageRef.onSnapshot((querySnap)=>{
              const allmsg =   querySnap.docs.map(docSanp=>{
               const data = docSanp.data()
               if(data.createdAt){
                   return {
                      ...docSanp.data(),
                      createdAt:docSanp.data().createdAt.toDate()
                  }
               }else {
                  return {
                      ...docSanp.data(),
                      createdAt:new Date()
                  }
               }
                  
              })
              setMessages(allmsg)
          })
  
          return ()=>{
            unSubscribe()
          }
  
          
        }, [email])
       const userTyping = (e) =>{ e.keyCode === 13 ? onSend() : setChatText(e.target.value); console.log(e.target.value)};
       const messageValid = (txt) => txt && txt.replace(/\s/g, '').length;
      //const userClickedInput = () => userClickedInputFn();
      useEffect(()=>{
        document.querySelector('#contentText').scrollTop = 9999999;
      },[messages])
      const onSend =() => {
        const msg = chatText
        const mymsg = {
            text: msg,
            sentBy:user.email,
            sentTo:email,
            createdAt:new Date()
        }
   
      
       const docid  = email > user.email ? user.email+ "-" + email : email+"-"+user.email 
        
       firebase.firestore().collection('chatrooms')
       .doc(docid)
       .collection('messages')
       .add({...mymsg,createdAt:firebase.firestore.FieldValue.serverTimestamp()})
       document.getElementById('chattextbox').value = ''
       setChatText("")
      }
      const selectMessage = e => {
        setChatText(e.message)        
      }

    return (
      
      <div  className={classes.content} id='contentText'>
              <div className={classes.select}>
                <Select options={options} placeholder='Selecione uma mensagem' onChange={selectMessage}/>
              </div>
          <div>
              <main id='chatview-container'>
              {
                messages.slice(0).reverse().map((_msg, _index) => {
                  return(
                  <div key={_index} className={_msg.sentTo === user.email ? classes.friendSent:classes.userSent }>
                    {_msg.text}
                  </div>
                  )
                })
              }
            </main>
            </div>            
            <div className={classes.chatTextBoxContainer}>            
                <TextField
                  placeholder='Digite sua mensagem' 
                  value={chatText}
                  onChange={(e) => userTyping(e)}
                  id='chattextbox' 
                  className={classes.chatTextBox}
                 
                  >
                </TextField>
                <Send onClick={onSend} className={classes.sendBtn}></Send>
               
            </div>          
        </div>
         
       
    )
}