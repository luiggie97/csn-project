import React, {useState, useEffect} from "react";
import MessagesDefaultForm from "./messages-form"
import firebase from 'firebase'
require('firebase/auth');

const MessagesDefault = () => {

  var [messagesObject, setMessagesObjects] = useState({})
  var [currentId, setCurrentId] = useState('')
  var [currentIdSearch, setCurrentIdSearch] = useState('')
  useEffect( async ()=>{
    const querySanp = await firebase.firestore().collection('default-message').get()
    const allMessages = querySanp.docs.map(docSnap=>{
      const data = docSnap.data()
      data.id = docSnap.id
      return data;
    })     
    if(allMessages != null)
      setMessagesObjects({
        ...allMessages
      })
  },[currentId,currentIdSearch])
  const onDelete = (idSearc)=>{
    if(window.confirm('Deseja realmente deletar a mensagem?')){
      firebase.firestore().collection("default-message").doc(idSearc).delete()
    }
    if(currentIdSearch == null)
    {
      setCurrentIdSearch("");
    }
    else
    {
      setCurrentIdSearch(null);
    }
  }
  const addOrEdit = obj =>{ 
    if(currentId==''){
      firebase
        .firestore()
        .collection('default-message').add(
          obj,
          err=>{        
              setCurrentId("")
          }
      )      
    }    
    else{
      firebase
      .firestore()
      .collection('default-message')
      .doc(currentIdSearch)
      .update(
        obj,  
        err=>{     
           
        }      
      )
    }   
    setCurrentId("")  
    if(currentIdSearch == null)
    {
      setCurrentIdSearch("");
    }
    else
    {
      setCurrentIdSearch(null);
    }
  }
   return (
     <>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4 text-center">Registro de Mensagens padrão</h1>
          <p class="lead"></p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-5">
          <MessagesDefaultForm {...({addOrEdit, currentId, currentIdSearch, messagesObject})}/>
        </div>
        <div className="col-md-7">
          <table className="table table-borderless table-stripped">
            <thead className="thead-light">
                <tr>
                  <th>Nome</th>
                  <th>Mensagem</th>
                  <th>Ações</th>
                </tr>
            </thead>
            <tbody>              
              {
                Object.keys(messagesObject).map(id=>{
                  return( 
                  <tr key={id}>
                      <td>{messagesObject[id].name}</td>
                      <td>{messagesObject[id].message}</td>
                      <td>
                          <a className="btn text-primary" onClick={()=>{setCurrentId(id); setCurrentIdSearch(messagesObject[id].id)}}>
                            <i className="fas fa-pencil-alt"></i>
                          </a>
                          <a className="btn text-danger" onClick={()=>{onDelete(messagesObject[id].id)}}>
                            <i className="far fa-trash-alt"></i>
                          </a>
                      </td>
                  </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    </>
   );
}
export default MessagesDefault