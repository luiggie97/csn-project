import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    img:{width:60,height:60,borderRadius:30,backgroundColor:"green"},

    text:{
        fontSize:18,
        marginLeft:15,
        color:'#082e7c',
    },
    root: {
        backgroundColor: theme.palette.background.paper,
        height: '87%',
        position: 'absolute',
        left: '0',
        width: '300px',
        boxShadow: '0px 0px 2px black'
      },
      listItem: {
        cursor: 'pointer'
      },
    mycard:{
        flexDirection:"row",      
        borderRadius: 20,     
        color: 'blue',
        textDecorationColor:'blue',      
        margin:3,
        padding:6,
        backgroundColor:"white",
        borderBottomWidth:1,
        borderBottomColor:'grey'
    },
    signOutBtn: {
    position: 'absolute',
    bottom: '0px',
    left: '0px',
    width: '300px',
    borderRadius: '0px',
    backgroundColor: '#227092',
    height: '35px',
    boxShadow: '0px 0px 2px black',
    color: 'white'
  },
  chatTextBoxContainer: {
    position: 'absolute',
    bottom: '15px',    
    boxSizing: 'border-box',
    overflow: 'auto',
    width: '90%',
    marginTop:'100px'
  },
  sendBtn: {
    color: 'blue',
    cursor: 'pointer',
    '&:hover': {
      color: 'gray'
    }
  },
  chatTextBox: {
    width: 'calc(100% - 25px)'
  },
  content: {
    paddingLeft: '7%',
    width: '100%',
    height: '90%',
    overflow: 'auto',   
    boxSizing: 'border-box',
    overflowY: 'scroll', 
  },
  mensagem:{
    height: '88%',
    position: 'absolute',    
    right:0,
    left:0,
    marginLeft: '20%',
  },
  userSent: {
    float: 'right',
    clear: 'both',
    padding: '20px',
    boxSizing: 'border-box',
    wordWrap: 'break-word',
    marginTop: '10px',
    backgroundColor: '#082e7c',
    color: 'white',
    width: '300px',
    borderRadius: '10px'
  },
  friendSent: {
    float: 'left',
    clear: 'both',
    padding: '20px',
    boxSizing: 'border-box',
    wordWrap: 'break-word',
    marginTop: '10px',
    backgroundColor: '#C0C0C0',
    color: 'white',
    width: '300px',
    borderRadius: '10px'
  },
  select: {
    marginTop: '10px',
    marginRight: '10px',
    position: 'absolute',
   // bottom: '15px',    
    boxSizing: 'border-box',
    //overflow: 'auto',
    width: '90%'
  }
}));