import React from "react";
import firebase from 'firebase'
require('firebase/auth')

export const UserInput = ({ user }) => {
  const [email, setEmail] = React.useState(user.email);

  const onUpdate = () => {
    const db = firebase.firestore()
    db.collection('users').doc(user.id).set({...user, email})
  }

  const onDelete = () => {
    const db = firebase.firestore()
    db.collection('users').doc(user.id).delete()
  }

  return (
    <>
      <input
        value={email}
        onChange={e => {
          setEmail(e.target.value);
        }}
      />
      <button onClick={onUpdate}>Updat</button>
      <button onClick={onDelete}>Delet</button>
    </>
  );
};
