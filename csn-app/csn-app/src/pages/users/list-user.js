import React from 'react';
import { UserInput } from "./update-delete";
import Table from "./table";
import firebase from 'firebase'
import ListUserComponentForm from './list-user-form';
import { MuiThemeProvider } from 'material-ui/styles';
require('firebase/auth')

export default class ListUserComponent extends React.Component {
  state = {
    data: [],
    editIdx: -1
  }; 
  
  componentWillMount = () => {
    firebase.auth().onAuthStateChanged(async _usr => {
        console.log(_usr);
      if(!_usr)
        this.props.history.push('/');
      else{
        const fetchData = async () => {
          const db = firebase.firestore();
          const data = await db.collection("users").get();
          console.log(data.docs)
          this.setState({data: data.docs.map(doc => ({ ...doc.data(), id: doc.id }))});
          console.log(this.state);     
        };
        fetchData();
      }  
  }  
  );
}

  handleRemove = i => {
    this.setState(state => ({
      data: state.data.filter((row, j) => j !== i)
    }));
  };

  startEditing = i => {
    this.setState({ editIdx: i });
  };

  stopEditing = () => {
    this.setState({ editIdx: -1 });
  };

   addOrEdit = obj =>{   
    firebase
      .auth()
      .createUserWithEmailAndPassword(obj.email, obj.password)
      .then(authRes => {
        const userObj = {
          email: authRes.user.email,
          mobile: obj.mobile,
          registration: obj.registration,
          fullName:obj.fullName
        };
        firebase
          .firestore()
          .collection('users')
          .doc(obj.email)
          .set(obj)
          .then(() => {
            
        }, dbErr => {
          console.log('Falha ao adicionar usuário ao banco de dados: ', dbErr);
          //this.setState({ signupError: 'Falha ao adicionar usuário' });
        });
    }, authErr => {
      console.log('Failed to create user: ', authErr);
      //this.setState({ signupError: 'Falha ao adicionar usuário' });
    });

  }


  handleChange = (e, name, i) => {
    const { value } = e.target;
    this.setState(state => ({
      data: state.data.map(
        (row, j) => (j === i ? { ...row, [name]: value } : row)
      )
    }));
  };
  render() {
    return (
    <MuiThemeProvider>    
    <div className="row">
    <div className="col-md-10 offset-md-1">
    <div id="container" style={{backgroundColor:'black', minHeight:'500px', minWidth:'500px'}}>
      <div style={{position:'fixed', backgroundColor:'red', minHeight:'300px', minWidth:'300px'}}></div>
      <div style={{position:'absolute', backgroundColor:'blue', minHeight:'100px', minWidth:'100px'}}></div>
    </div>
      <div style={{}} className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">Registro de Usuários</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <ListUserComponentForm 
            onSubmit={submission =>
              this.setState({
                data: [...this.state.data, submission]
              })}></ListUserComponentForm>
        </div>
        <div className="col-md-12">
          <div>
          <Table
            handleRemove={this.handleRemove}
            startEditing={this.startEditing}
            editIdx={this.state.editIdx}
            stopEditing={this.stopEditing}
            handleChange={this.handleChange}
            data={this.state.data}
            header={[
            {
              name: "Nome",
              prop: "fullName"
            },
            {
              name: "Telefone",
              prop: "mobile"
            },
            {
              name: "E-mail",
              prop: "email"
            },
            {
              name: "Matrícula",
              prop: "registration"
            },        
          ]}
        />
          </div>
        </div>
      </div>

    </div>
  </div>
  </MuiThemeProvider>  

    );
  }

}

