import React, { useState, useEffect } from 'react';
import { UserInput } from "./update-delete";
import { Form, Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap"
import firebase from 'firebase'
require('firebase/auth')
export default class ListUserComponentForm extends React.Component {
    state = {
        fullName: ' ',
        mobile: ' ',
        email: ' ',
        password: ' ',
        registration:' '
    }

    change = e => {        
        this.setState({
          [e.target.name]: e.target.value
        });
      };
//* ON SUBMIT*
      onSubmit = e => {
        e.preventDefault(); 
        
          this.props.onSubmit(this.state);          
          this.setState({
                fullName: "" ,
                mobile: "",
                email: "",
                password: "",
                registration:""
          });
        
      };
      render() {
        return (
            <Form autoComplete="off" onSubmit={e => this.onSubmit(e)}>
            <div className="form-group input-group">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-user"></i>
                    </div>
                </div>
                <input className="form-control" placeholder="Nome Completo" name="fullName"
                    value={this.state.fullName} onChange={e => this.change(e)}/>                    
            </div>  
            <div className="row pt-3">
                <div className="col-md-6">
                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i className="fas fa-mobile-alt"></i>
                        </div>
                    </div>
                    <input className="form-control" placeholder="Celular" name="mobile"
                        value={this.state.mobile} onChange={e => this.change(e)} />
                        
                </div>                
                </div>
                <div className="col-md-6">
                    <div className="form-group input-group">
                        <div className="input-group-prepend">
                            <div className="input-group-text">
                                <i className="fas fa-envelope"></i>
                            </div>
                        </div>
                        <input className="form-control" placeholder="E-mail" name="email"
                            value={this.state.email} onChange={e => this.change(e)} />
                            
                    </div>
                </div>
            </div>
            <div className="row pt-3">
                <div className="col-md-6">
                <div className="form-group input-group">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <i className="fas fa-building"></i>
                        </div>
                    </div>
                    <input className="form-control" placeholder="Matrícula" name="registration"
                        value={this.state.registration} onChange={e => this.change(e)}/>                        
                </div>                
                </div>
                <div className="col-md-6">
                    <div className="form-group input-group">
                        <div className="input-group-prepend">
                            <div className="input-group-text">
                                <i className="fas fa-lock"></i>
                            </div>
                        </div>
                        <input className="form-control" type="password" placeholder="Senha" name="password"
                            value={this.state.password} onChange={e => this.change(e)}/>                            
                    </div>
                </div>
            </div>
            <div className="form-group pt-3 col-md-3 pl-0">
                <input type="submit" value="Salvar" className="btn btn-primary btn-block"/>
            </div>
        </Form>
          
        );
      }   
}