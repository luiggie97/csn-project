import React from "react";
import UserGroupComponent from './user-group';

function UserGroupRoot(){
    return (
        <div className="row">
            <div className="col-md-8 offset-md-2">
                <UserGroupComponent></UserGroupComponent>
            </div>
        </div>
    );
}

export default UserGroupRoot;