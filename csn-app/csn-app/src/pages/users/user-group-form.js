import React,{useState, useEffect,Component} from "react";
import Select from 'react-select';
import firebase from 'firebase'
require('firebase/auth')

const UserGroupForm = (props) => {
const initialFieldValues = {
    name:'',
    details:'',
    users:''    
}

var [values, setValues] = useState(initialFieldValues)
var [options, setOptions] = useState(null)
var [defaultValues, setDefaultValues] = useState(null)

useEffect( async ()=>{
    const querySanp = await firebase.firestore().collection('users').get()
    const allUsers = querySanp.docs.map(docSnap=>{
        const data= {
          value: docSnap.data().email,
          label: docSnap.data().email
        }
        return data;
      })  
      setOptions(allUsers); 
      
  })

useEffect( ()=>{    
    if(props.currentId=="" )
    {
        setValues({
            ...initialFieldValues
        });
        setOptions(null);
    }        
    else
       
        setValues({
            ...props.userGroupsObject[props.currentId]
        })
       
},[props.currentId, props.currentIdSearch,props.userGroupsObject])

const handleSelect = e =>{
    var usersGroup = '';
    e.map(users=>{
        if(usersGroup =='')
            usersGroup = users.value ;
        else
            usersGroup = usersGroup+'-'+ users.value ;
    })  
    setValues({
        ...values,
        users: usersGroup
    })
    console.log(values);  
}

const handleInputChange = e =>{
    var {name, value } = e.target
    setValues({
        ...values,
        [name]: value
    })
    console.log(values);  
}
const handleFormSubmit = e =>{
    e.preventDefault();
    props.addOrEdit(values);   
}

    return (
      <form autoComplete="off" onSubmit={handleFormSubmit}>                
          <div className="form-row">
            <div className="form-group input-group col-md-12">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-file-signature"></i>
                    </div>
                </div>
                <input className="form-control" placeholder="Nome" name="name"
                    value={values.name}
                    onChange={handleInputChange}
                />
            </div>    
          </div>
          <div className="form-row">
            <div className="form-group input-group col-md-12">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i className="fas fa-comment-alt"></i>
                    </div>
                </div>
                <input className="form-control" placeholder="Detalhes" name="details"
                    value={values.details}
                    onChange={handleInputChange}
                />                
            </div>    
          </div>
          <div className="form-row">
            <div className="form-group input-group col-md-12">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <i class="fas fa-user"></i>
                    </div>
                </div>
                <Select options={options} className="w-75 basic-multi-select"  classNamePrefix="select" isMulti onChange={handleSelect} />                          
            </div>    
          </div>
          
          <div className="form-row">
            <div className="form-group col-md-3">
                <input type="submit" value={props.currentId==''?"Salvar":"Atualizar"} className="btn btn-primary btn-block"/>
            </div>    
          </div>
          
      </form>
    );
 }
 export default UserGroupForm