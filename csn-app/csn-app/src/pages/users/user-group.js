import React, {useState, useEffect} from "react";
import UserGroupForm from "./user-group-form";
import firebase from 'firebase';
require('firebase/auth');

const UserGroupComponent = () => {

  var [userGroupsObject, setUserGroupsObjects] = useState({})
  var [currentId, setCurrentId] = useState('')
  var [currentIdSearch, setCurrentIdSearch] = useState('')
  useEffect( async ()=>{
    const querySanp = await firebase.firestore().collection('user-group').get()
    const allGroups = querySanp.docs.map(docSnap=>{
      const data = docSnap.data()
      data.id = docSnap.id
      return data;
    })     
    if(allGroups != null)
    setUserGroupsObjects({
        ...allGroups
      })
  },[currentId,currentIdSearch])
  const onDelete = (idSearc)=>{
    if(window.confirm('Deseja realmente deletar o grupo?')){
      firebase.firestore().collection("user-group").doc(idSearc).delete()
    }
    if(currentIdSearch == null)
    {
      setCurrentIdSearch("");
    }
    else
    {
      setCurrentIdSearch(null);
    }
  }
  const addOrEdit = obj =>{ 
    if(currentId==''){
      firebase
        .firestore()
        .collection('user-group').add(
          obj,
          err=>{        
              setCurrentId("")
          }
      )      
    }    
    else{
      firebase
      .firestore()
      .collection('user-group')
      .doc(currentIdSearch)
      .update(
        obj,  
        err=>{     
           
        }      
      )
    }   
    setCurrentId("")  
    if(currentIdSearch == null)
    {
      setCurrentIdSearch("");
    }
    else
    {
      setCurrentIdSearch(null);
    }
  }
   return (
     <>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h1 class="display-4 text-center">Registro de Grupos de usuários</h1>
          <p class="lead"></p>
        </div>
      </div>
      <div className="row">
        <div className="col-md-5">
          <UserGroupForm {...({addOrEdit, currentId, currentIdSearch, userGroupsObject})}/>
        </div>
        <div className="col-md-7">
          <table className="table table-borderless table-stripped">
            <thead className="thead-light">
                <tr>
                  <th>Nome</th>
                  <th>Detalhes</th>
                  <th>Usuários</th>
                  <th>Ações</th>
                </tr>
            </thead>
            <tbody>              
              {
                Object.keys(userGroupsObject).map(id=>{
                  return( 
                  <tr key={id}>
                      <td>{userGroupsObject[id].name}</td>
                      <td>{userGroupsObject[id].details}</td>
                      <td>{                      
                          userGroupsObject[id].users
                        }
                      </td>
                      <td>
                          <a className="btn text-primary" onClick={()=>{setCurrentId(id); setCurrentIdSearch(userGroupsObject[id].id)}}>
                            <i className="fas fa-pencil-alt"></i>
                          </a>
                          <a className="btn text-danger" onClick={()=>{onDelete(userGroupsObject[id].id)}}>
                            <i className="far fa-trash-alt"></i>
                          </a>
                      </td>
                  </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    </>
   );
}
export default UserGroupComponent;